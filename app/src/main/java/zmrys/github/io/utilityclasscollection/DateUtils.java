package zmrys.github.io.utilityclasscollection;


import android.util.Log;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;


/**
 * Created by myolwin00 on 8/15/17.
 */

public class DateUtils {
    public static String getFormattedCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(System.currentTimeMillis());
    }

    public static Date getCurrentDate() {
        return new Date(System.currentTimeMillis());
    }

    public static String getFormattedCurrentTimeForComment() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz");
        return formatter.format(System.currentTimeMillis());
    }

    public static String getFormattedServerTime() {
        //2017-08-30T16:46:43.000Z
        //yyyy-MM-dd'T'HH:mm:ss.SSSz
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(System.currentTimeMillis());


    }

    public static String getCurrentDayOfWeek() {
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE");
        return formatter.format(System.currentTimeMillis());
    }

    public static List<String> getDaysOfWeekList() {
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
        String[] days = dateFormatSymbols.getWeekdays();
        return Arrays.asList(days);
    }

    public static HashMap<Integer, String> findOffDays(List<String> openDaysList) {
        HashMap<Integer, String> offDays = new HashMap<>();
        List<String> daysOfWeek = DateUtils.getDaysOfWeekList();
        for (int i = 1; i < daysOfWeek.size(); i++) {
            if (!openDaysList.contains(daysOfWeek.get(i))) {
                offDays.put(i - 1, daysOfWeek.get(i));
            }
        }
        return offDays;
    }

    public static String secondsToHourMinuteSecond(int seconds) {
        String timeStr;
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        if (h > 0)
            timeStr = String.format("%d:%02d:%02d", h, m, s);
        else
            timeStr = String.format("%02d:%02d", m, s);

        return timeStr;
    }


    public static String getServerTimeByDate(Long milliTimes) {
        //2017-08-30T16:46:43.000Z
        //yyyy-MM-dd'T'HH:mm:ss.SSSz
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(milliTimes);

    }

    public static String getCurrentYearMonthDay() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String getCurrentYear() {
        return String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
    }

    public static int getCurrentMonth() {
        return Calendar.getInstance().get(Calendar.MONTH) + 1;
    }

    public static String getDayMonthYearByDate(Date date) {
        return new SimpleDateFormat("dd-MM-yyyy").format(date);
    }


    public static List<String> getYearList(int retrieveYearCount) {
        List<String> years = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int endYear = currentYear - retrieveYearCount;
        for (int i = currentYear; i >= endYear; i--) {
            years.add(i + "");
        }
        return years;
    }


    public static boolean isCurrentDateBetweenFromAndTo(String from, String to) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date fromDate = sdf.parse(from);
            Date toDate = sdf.parse(to);
            Date currentDate = sdf.parse(getCurrentYearMonthDay());
            if (currentDate.compareTo(fromDate) >= 0 && currentDate.compareTo(toDate) <= 0) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static long getLeftDaysToUploadDemand(String toDateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long days = 0;
        try {
            Date toDate = sdf.parse(toDateString);
            Date nowDate = sdf.parse(getCurrentYearMonthDay());
            days = (toDate.getTime() - nowDate.getTime()) / (24 * 60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return days;
    }

    private static long getLeftMonth(String toDateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long month = 0;
        try {
            Date toDate = sdf.parse(toDateString);
            Date nowDate = sdf.parse(getCurrentYearMonthDay());
            month = toDate.getTime() - nowDate.getTime() / (30 * 24 * 60 * 60 * 1000);
            return month;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return month;
    }


    // 2004-06-14T19:GMT20:30Z
    // 2004-06-20T06:GMT22:01Z

    // http://www.cl.cam.ac.uk/~mgk25/iso-time.html
    //
    // http://www.intertwingly.net/wiki/pie/DateTime
    //
    // http://www.w3.org/TR/NOTE-datetime
    //
    // Different standards may need different levels of granularity in the date and
    // time, so this profile defines six levels. Standards that reference this
    // profile should specify one or more of these granularities. If a given
    // standard allows more than one granularity, it should specify the meaning of
    // the dates and times with reduced precision, for example, the result of
    // comparing two dates with different precisions.

    // The formats are as follows. Exactly the components shown here must be
    // present, with exactly this punctuation. Note that the "T" appears literally
    // in the string, to indicate the beginning of the time element, as specified in
    // ISO 8601.

    //    Year:
    //       YYYY (eg 1997)
    //    Year and month:
    //       YYYY-MM (eg 1997-07)
    //    Complete date:
    //       YYYY-MM-DD (eg 1997-07-16)
    //    Complete date plus hours and minutes:
    //       YYYY-MM-DDThh:mmTZD (eg 1997-07-16T19:20+01:00)
    //    Complete date plus hours, minutes and seconds:
    //       YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)
    //    Complete date plus hours, minutes, seconds and a decimal fraction of a
    // second
    //       YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)

    // where:

    //      YYYY = four-digit year
    //      MM   = two-digit month (01=January, etc.)
    //      DD   = two-digit day of month (01 through 31)
    //      hh   = two digits of hour (00 through 23) (am/pm NOT allowed)
    //      mm   = two digits of minute (00 through 59)
    //      ss   = two digits of second (00 through 59)
    //      s    = one or more digits representing a decimal fraction of a second
    //      TZD  = time zone designator (Z or +hh:mm or -hh:mm)

    public static Date parse(String input) throws ParseException {
        //NOTE: SimpleDateFormat uses GMT[-+]hh:mm for the TZ which breaks
        //things a bit.  Before we go on we have to repair this.
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz");

        //this is zero time so we need to add that TZ indicator for
        if (input.endsWith("Z")) {
            input = input.substring(0, input.length() - 1) + "GMT+00:00";
        } else {
            int inset = 6;

            String s0 = input.substring(0, input.length() - inset);
            String s1 = input.substring(input.length() - inset, input.length());

            input = s0 + "GMT" + s1;
        }
        return df.parse(input);
    }

    public static String getFormattedDate(String date) {
        Date d;
        DateFormat df;
        StringTokenizer st;
        String tmp;
        try {
            d = parse(date);
            Log.i("COMMENT DATE ", d.toString());
            df = new SimpleDateFormat("d MMM yyyy h:mm a");
            //st = new StringTokenizer(df.format(d).toUpperCase());
            Log.i("CONVERT DATE ", df.format(d).toUpperCase());
            //tmp = st.nextToken() + " " + MyHealthCareApp.getContext().getResources().getStringArray(R.array.month_array)[Integer.parseInt(st.nextToken()) - 1] + " " + st.nextToken();
            return df.format(d).toUpperCase();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getFormattedTime(String date) {
        Date d = null;
        DateFormat df = null;
        StringTokenizer st;
        String tmp;
        try {
            d = parse(date);
            df = new SimpleDateFormat("h:mm#a");//yyyy-MMMM-dd h:mma");
            tmp = df.format(d).toUpperCase();
            st = new StringTokenizer(tmp, "#");
            tmp = st.nextToken();
            if (st.nextToken().equalsIgnoreCase("PM")) {
                tmp += "pm";
            } else {
                tmp += "am";
            }
            return tmp;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }


    //2017-04-28T05:09:03.000Z
    public static String toString(Date date) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");

        TimeZone tz = TimeZone.getTimeZone("UTC");

        df.setTimeZone(tz);

        String output = df.format(date);

        int inset0 = 9;
        int inset1 = 6;

        String s0 = output.substring(0, output.length() - inset0);
        String s1 = output.substring(output.length() - inset1, output.length());

        String result = s0 + s1;

        result = result.replaceAll("UTC", "+00:00");

        return result;

    }

    public static String getFormattedDateAndTime(String date) {
        return getFormattedDate(date) + " at " + getFormattedTime(date);
    }

}
