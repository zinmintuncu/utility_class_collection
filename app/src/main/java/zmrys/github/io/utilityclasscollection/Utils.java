package zmrys.github.io.utilityclasscollection;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;


/**
 * Created by nwt on 02/08/2018.
 */

public class Utils {

    public static void saveHashMap(Context context, String key, HashMap<String, Integer> inputMap) {
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        if (pSharedPref != null) {
            JSONObject jsonObject = new JSONObject(inputMap);
            String jsonString = jsonObject.toString();
            Log.d("VIEW_COUNT ", jsonString);
            SharedPreferences.Editor editor = pSharedPref.edit();
            editor.remove(key).apply();
            editor.putString(key, jsonString);
            editor.commit();
        }
    }

    public static HashMap<String, Integer> loadHashMap(Context context, String key) {
        HashMap<String, Integer> outputMap = new HashMap<String, Integer>();
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            if (pSharedPref != null) {
                String jsonString = pSharedPref.getString(key, (new JSONObject()).toString());
                JSONObject jsonObject = new JSONObject(jsonString);
                Iterator<String> keysItr = jsonObject.keys();
                while (keysItr.hasNext()) {
                    String k = keysItr.next();
                    Integer v = (Integer) jsonObject.get(k);
                    outputMap.put(k, v);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputMap;
    }

    public static String loadJSON(Context context, String key) {
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return pSharedPref.getString(key, (new JSONObject()).toString());
    }

    public static void clearData(Context context, String key) {
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pSharedPref.edit();
        editor.putString(key, (new JSONObject()).toString());
        editor.apply();
    }



    public static int getVersionCode(Context context) {

        int verCode = 0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            verCode = pInfo.versionCode;
            Log.i("Version Code ", verCode + "");

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return verCode;
    }

    public static void saveUpdateAvailable(Context context, boolean isAvailable) {
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pSharedPref.edit();
        editor.putBoolean("UpdateAvailable", isAvailable);
        editor.apply();

        Log.i("UPDATE STATUS ", Utils.getUpdateAvailable(context) + "");
    }

    public static boolean getUpdateAvailable(Context context) {
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return pSharedPref.getBoolean("UpdateAvailable", false);
    }

    public static void saveForceUpdate(Context context, boolean isForceUpdate) {
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pSharedPref.edit();
        editor.putBoolean("ForceUpdate", isForceUpdate);
        editor.apply();
    }

    public static boolean getForceUpdate(Context context) {
        SharedPreferences pSharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return pSharedPref.getBoolean("ForceUpdate", false);
    }

    public static boolean validCellPhone(String number) {
        return android.util.Patterns.PHONE.matcher(number).matches();
    }

}
