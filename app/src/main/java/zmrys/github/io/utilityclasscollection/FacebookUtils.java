package zmrys.github.io.utilityclasscollection;

/**
 * Created by SMM on 1/31/2018.
 */

public class FacebookUtils {
    public static String getFormatedLikeCount(int count) {
        String stringCount = " ";
        if (count < 1000)
            stringCount += count == 0 || count == 1 ? count + " Like" : count + " Likes";
        else
            stringCount += count % 1000 != 0 ? Math.round((count / 1000.0) * 10.0) / 10.0 + "K Likes" :
                    (count / 1000) + " K Likes";
        return stringCount;
    }

    public static String getFormatedCommentCount(int comment) {
        String stringCount = " ";
        if (comment < 1000)
            stringCount += comment == 0 || comment == 1 ? comment + " Comment" : comment + " Comments";
        else
            stringCount += comment % 1000 != 0 ? Math.round((comment / 1000.0) * 10.0) / 10.0 + "K Comments" :
                    (comment / 1000) + "K Comments";
        return stringCount;
    }

    public static String getFormatedReviewCount(int comment) {
        String stringCount = "";
        if (comment < 1000)
            stringCount += comment == 0 || comment == 1 ? comment + " Review" : comment + " Reviews";
        else
            stringCount += comment % 1000 != 0 ? Math.round((comment / 1000.0) * 10.0) / 10.0 + "K Reviews" :
                    (comment / 1000) + "K Reviews";
        return stringCount;
    }

}
